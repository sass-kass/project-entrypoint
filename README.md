# Why 42?

<h1>Трек DATA</h1>

Инструкция по установке и запуску проектов по пунктам со ссылками на исходный код и Read.me каждого проекта по порядку: <br />

<h2>1. ML-API (установка и запуск обязательны) </h2> <br />
- Сссылка на исходный код: https://gitlab.com/Egermeisterrr/more.tech.4.0.ml_api <br />

<h2>2. Main-API (установка и запуск не обязательны)</h2> <br />
- Сссылка на исходный код: https://github.com/miuroku/news-vtb-server <br />
- Задеплоено на: https://news-vtb-server-production.up.railway.app/ <br />
- Ссылка на Workspace c доступными готовыми запросами: https://app.getpostman.com/join-team?invite_code=0e8cdb67d96d756cd391ce61fc382625&target_code=ed17671ae58fdc9b8b54989f28a79242 <br />  
- (так как не успелся деплой ML-API сервиса рекомендуется установить этот сервер по инструкции из Read.me) <br />

<h2>3. Android приложение (установка и запуск обязательны)</h2> <br />
- Сссылка на исходный код: https://gitlab.com/Egermeisterrr/more.tech.4.0.android <br />
